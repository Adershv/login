package com.vmoksha.login;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.vmoksha.login.db.AppDatabase;
import com.vmoksha.login.model.User;

import static com.vmoksha.login.R.id.forgotPass;


public class LoginFragment extends Fragment {

    TextInputEditText edtemail;
    TextInputEditText edtpassword;
    String email,password;
    Button btnLogin;
    AppDatabase db;
    LoginViewModel loginViewModel;
    TextView forgotPass;
    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtemail = view.findViewById(R.id.edtEmail);
        edtpassword = view.findViewById(R.id.edtPassword);
        forgotPass = view.findViewById(R.id.forgotPass);
        btnLogin = view.findViewById(R.id.btnsignIn);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email =  edtemail.getText().toString();
                password = edtpassword.getText().toString();

                if (email.equals(""))edtemail.setError("Null not allowed");
                if (password.equals(""))edtpassword.setError("Null not allowed");

                if (Validation.isValidEmail(email)){
                    if (Validation.isValidPassword(password)){
                        User user =new User("",email,password);
                        loginViewModel.login(email,password).observe(getActivity(), new Observer<User>() {
                            @Override
                            public void onChanged(User user) {
                                if (user == null)
                                    Toast.makeText(getActivity(),"Login Failed",Toast.LENGTH_LONG).show();
                                else{
                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    intent.putExtra("name", user.getName());
                                    intent.putExtra("email",user.getEmail());
                                    startActivity(intent);
                                    Toast.makeText(getActivity(),"Login Success"+user.name,Toast.LENGTH_LONG).show();
                                    getActivity().finish();
                                }
                            }
                        });
                    }else {
                        edtpassword.setError("Password not valid");
                    }
                }else {
                    edtemail.setError("Email not valid");
                }







            }
        });


        forgotPass.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.forgotpasswordlayout);
                final EditText edtForgotPass = dialog.findViewById(R.id.edtEmail);
                Button submit = dialog.findViewById(R.id.btnSubmit);
                Button cancel = dialog.findViewById(R.id.btnCancel);
                dialog.setTitle("Forgot Pass");
                final TextView txtPass = dialog.findViewById(R.id.txtPass);

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Validation.isValidEmail(edtForgotPass.getText().toString()))
                        loginViewModel.forgotPass(edtForgotPass.getText().toString()).observe(getActivity(), new Observer<User>() {
                            @Override
                            public void onChanged(User user) {
                                if (user == null){
                                    Toast.makeText(getActivity(),"Wrong Email",Toast.LENGTH_LONG).show();

                                    txtPass.setText("Wrong Email");
                                }
                                else{


                                    txtPass.setText("password  "+user.password);
                                    Toast.makeText(getActivity(),"Please check password  "+user.password,Toast.LENGTH_LONG).show();

                                }
                            }
                        });
                        else{

                            txtPass.setText("Not a valid email");
                        }

                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });


    }


}
