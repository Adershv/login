package com.vmoksha.login.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.vmoksha.login.model.User;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    LiveData<List<User>> getAll();

    @Query("SELECT * FROM user WHERE uid IN (:userIds)")
    List<User> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user WHERE email = :email AND " +
            "password = :password LIMIT 1")
    LiveData<User> findByEmailAndPassword(String email, String password);

    @Query("SELECT * FROM user WHERE email = :email" +
            " LIMIT 1")
    LiveData<User> findByEmail(String email);

    @Query("SELECT * FROM user WHERE email = :email" +
            " LIMIT 1")
    User isEmailExist(String email);

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(User user);
}