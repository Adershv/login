package com.vmoksha.login;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.vmoksha.login.dao.UserDao;
import com.vmoksha.login.db.AppDatabase;
import com.vmoksha.login.model.User;

import java.util.List;

public class LoginViewModel extends AndroidViewModel {

    private String TAG = "LoginViewModel";
    private UserDao userDao;
    private AppDatabase appDatabase;
    private LiveData<List<User>> user;
    private LiveData<User> loginedUser = null;


    public LoginViewModel(Application application) {
        super(application);

        appDatabase = AppDatabase.getDatabase(application);
        userDao = appDatabase.userDao();
        user = userDao.getAll();
    }

    public void insert(User user) {
        new InsertAsyncTask(userDao).execute(user);
    }

    LiveData<List<User>> getAllUsers() {
        return user;
    }

    LiveData<User> login(String email, String password) {
        loginedUser = userDao.findByEmailAndPassword(email,password);

        return loginedUser;

    }
    LiveData<User> forgotPass(String email) {
        loginedUser = userDao.findByEmail(email);

        return loginedUser;

    }
    LiveData<User> findByemail(String email) {
        loginedUser = userDao.findByEmail(email);

        return loginedUser;

    }





    public void update(User user) {
        new UpdateAsyncTask(userDao).execute(user);
    }

    public void delete(User user) {
        new DeleteAsyncTask(userDao).execute(user);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.i(TAG, "ViewModel Destroyed");
    }

    private class OperationsAsyncTask extends AsyncTask<User, Void, Void> {

       UserDao mAsyncTaskDao;

        OperationsAsyncTask(UserDao dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(User... notes) {
            return null;
        }
    }

    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(UserDao userDao) {
            super(userDao);
        }

        @Override
        protected Void doInBackground(User... user) {
            mAsyncTaskDao.insert(user[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplication(),"User Created",Toast.LENGTH_LONG).show();
        }
    }

    private class UpdateAsyncTask extends OperationsAsyncTask {

        UpdateAsyncTask(UserDao userDao) {
            super(userDao);
        }

        @Override
        protected Void doInBackground(User... user) {
           // mAsyncTaskDao.update(notes[0]);
            return null;
        }
    }


    private class DeleteAsyncTask extends OperationsAsyncTask {

        public DeleteAsyncTask(UserDao userDao) {
            super(userDao);
        }

        @Override
        protected Void doInBackground(User... user) {
            mAsyncTaskDao.delete(user[0]);
            return null;
        }
    }
}