package com.vmoksha.login;

public class Validation {
    static boolean isValidEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public static boolean isValidPassword(String str)
    {
        return ((str != null)
                && (!str.equals(""))
                && (!(str.length() <6))
                && (str.matches("^[a-zA-Z]*$")));
    }
}

