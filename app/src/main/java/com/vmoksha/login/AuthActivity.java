package com.vmoksha.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.vmoksha.login.model.User;

import java.util.List;

public class AuthActivity extends AppCompatActivity {

    private TabLayout tabLayout ;
    private ViewPager viewpager;
    private ViewPageAdapter viewPageAdapter ;

    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
//        loginViewModel.getAllUsers().observe(this, new Observer<List<User>>() {
//            @Override
//            public void onChanged(List<User> users) {
//                Toast.makeText(AuthActivity.this,users.get(0).email,Toast.LENGTH_LONG).show();
//            }
//        });

        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager());

        tabLayout = findViewById(R.id.selectPartstabs);
        viewpager = findViewById(R.id.viewpager);

        viewPageAdapter.addFragment(new LoginFragment(),"SIGN IN");

        viewPageAdapter.addFragment(new RegistrationFragment(),"SIGN UP");

        viewpager.setAdapter(viewPageAdapter);
        tabLayout.setupWithViewPager(viewpager);

    }
}
