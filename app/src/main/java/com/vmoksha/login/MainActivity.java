package com.vmoksha.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Intent intent;
    String name,email;
    TextView txtname,txtemail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtname  = findViewById(R.id.txtName);
        txtemail = findViewById(R.id.txtEmail);


        intent = getIntent();

        name = intent.getExtras().getString("name");
        email = intent.getExtras().getString("email");

        txtname.setText(name);
        txtemail.setText(email);


    }
}
