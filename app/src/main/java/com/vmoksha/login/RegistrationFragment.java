package com.vmoksha.login;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.vmoksha.login.db.AppDatabase;
import com.vmoksha.login.model.User;

import java.util.ArrayList;
import java.util.List;


public class RegistrationFragment extends Fragment {

    TextInputEditText edtname;
    TextInputEditText edtemail;
    TextInputEditText edtpassword;
    String name,email,password;
    List<User> userList = new ArrayList<>();
    TextView status;

    Button signUp;
    AppDatabase db;
    LoginViewModel loginViewModel;
     boolean isEmailExist = false;
    public RegistrationFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);



//         db = Room.databaseBuilder(getActivity().getApplicationContext(),
//                AppDatabase.class, "database-name").build();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loginViewModel.getAllUsers().observe(getActivity(), new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                //Toast.makeText(getActivity(),users.get(3).email,Toast.LENGTH_LONG).show();
                //Log.d("RegFragment",users.get(3).email);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registration, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edtname = view.findViewById(R.id.edtname);
        edtemail = view.findViewById(R.id.edtEmail);
        edtpassword = view.findViewById(R.id.edtPassword);
        signUp = view.findViewById(R.id.btnSignup);

        status = view.findViewById(R.id.status);


        registerUser();


    }

    private void registerUser(){

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status.setVisibility(View.GONE);
                email =  edtemail.getText().toString();
                name  =  edtname.getText().toString();
                password = edtpassword.getText().toString();


                if (email.equals(""))edtemail.setError("Null not allowed");
                if (name.equals(""))edtname.setError("Null not allowed");
                if (password.equals(""))edtpassword.setError("Null not allowed");

                if (!email.equals("")&& !name.equals("")&& !password.equals("") ){
                    if (Validation.isValidPassword(password)){
                        if (Validation.isValidEmail(email) ){

                            loginViewModel.findByemail(email).observe(getActivity(), new Observer<User>() {
                                @Override
                                public void onChanged(User user) {
                                    if (user!= null){

                                    }else{
                                        User userdata = new User(name,email,password);
                                        loginViewModel.insert(userdata);
                                        status.setText("Registerd Successfully");
                                        status.setVisibility(View.VISIBLE);
                                        edtemail.setText("");
                                        edtname.setText("");
                                        edtpassword.setText("");
                                    }
                                }

                            });


                        }else {
                            edtemail.setError("Not a Valid Email");
                            Toast.makeText(getActivity(),"Not a Valid Email",Toast.LENGTH_LONG).show();
                        }

                    }else{
                        edtpassword.setError("Not a Valid Password");
                        Toast.makeText(getActivity()," Password not valid",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getActivity(),"Fields Should not be empty",Toast.LENGTH_LONG).show();
                }






            }
        });

    }

}
